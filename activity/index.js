// #3
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json))

// #4
		fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {
	json.map(todos => console.log(todos.title))
	})


// #5
fetch("https://jsonplaceholder.typicode.com/todos/6")
.then((response) => response.json())
.then((json) => console.log(json))

// #6
	fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {
	json.forEach(todos => console.log(todos.title))
	})

// #7

	fetch("https://jsonplaceholder.typicode.com/todos", {
		method:"POST",
		headers: {
			"Content-Type": "application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend/server
		body: JSON.stringify({
			title: "New Item",
			body: "This is a new item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// #8

	fetch("https://jsonplaceholder.typicode.com/todos/7", {
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Update Item",
			body: "Update the item #7",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// #9

	fetch("https://jsonplaceholder.typicode.com/todos/8", {
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "New data structure",
			description: "Changed the data structure",
			status: "completed",
			date_completed: "8/26/22",
			user_id: 1111
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// #10

	fetch("https://jsonplaceholder.typicode.com/todos/8", {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "This is the new title",
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// #11

	fetch("https://jsonplaceholder.typicode.com/todos/10", {
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// #12
fetch("https://jsonplaceholder.typicode.com/todos/11", {
	method: "DELETE"
})